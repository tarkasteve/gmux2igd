#include <stdio.h>
#include <sys/io.h>

#define PORT_SWITCH_DISPLAY 0x710
#define PORT_SWITCH_SELECT 0x728
#define PORT_SWITCH_DDC 0x740
#define PORT_DISCRETE_POWER 0x750

int main(int argc, char **argv)
{
    if (iopl(3) < 0) {
        perror ("No IO permissions");
        return 1;
    }

    printf("Discrete power is %x\n", inb(PORT_DISCRETE_POWER));
    printf("Switch select is %x\n", inb(PORT_SWITCH_SELECT));
    printf("Display switch is is %x\n", inb(PORT_SWITCH_DISPLAY));
    printf("DCC switch is %x\n", inb(PORT_SWITCH_DDC));

    return 0;
}
